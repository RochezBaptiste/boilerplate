import { ReactNode } from "react";
import { CssBaseline } from "@mui/material";
import { BrowserRouter } from "react-router-dom";

interface IParams {
    children: ReactNode;
}

export const AppProviders = (props: IParams) => {
    const { children } = props;

    return (
        <BrowserRouter>
            <CssBaseline />
            {children}
        </BrowserRouter>
    );
};
