import { Fragment, ReactNode } from "react";
import { Typography } from "@mui/material";

interface IParam {
    children: ReactNode;
}

export const Layout = (props: IParam) => {
    const { children } = props;

    return (
        <Fragment>
            <div>{children}</div>
            <Typography>this is the footer</Typography>
        </Fragment>
    );
};
