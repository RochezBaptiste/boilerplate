import { Component, ReactNode } from "react";
import { GlobalError } from "./GlobalError/GlobalError";

interface IState {
    hasError: boolean;
}

interface IParams {
    children: ReactNode;
}

export class ErrorBoundary extends Component<IParams, IState> {
    state = { hasError: false };

    static getDerivedStateFromError() {
        return { hasError: true };
    }

    render() {
        const { state, props } = this;
        if (state.hasError) {
            return <GlobalError retry={() => this.setState({ hasError: false })} />;
        }

        return props.children;
    }
}
