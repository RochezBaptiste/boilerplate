import { useNavigate } from "react-router-dom";
import { URLS } from "../../../constants";
import { Button, Typography } from "@mui/material";
import { Fragment } from "react";

interface IParams {
    retry(): void;
}

export const GlobalError = (props: IParams) => {
    const navigate = useNavigate();
    const { retry } = props;

    const goToMain = () => {
        navigate(URLS.DEFAULT_PAGE_URL);
        retry();
    };

    return (
        <Fragment>
            <Typography>This is an error</Typography>
            <Button variant="contained" onClick={goToMain}>
                Click me
            </Button>
        </Fragment>
    );
};
