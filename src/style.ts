import { SxProps } from "@mui/material/styles";

const root: SxProps = {
    bgcolor: "#fafafa",
};

const container: SxProps = {
    minHeight: "100vh",
};

export default { root, container };
