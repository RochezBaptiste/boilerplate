import { App } from "./App";
import { createRoot } from "react-dom/client";
import { theme } from "@styles/themeConfiguration";
import { ThemeProvider } from "@mui/material/styles";

const container = document.getElementById("root");
const root = createRoot(container!);
root.render(
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>
);
