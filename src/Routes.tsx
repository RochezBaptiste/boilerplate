import { Routes as Switch, Route } from "react-router-dom";
import { ReactNode, FC } from "react";
import { URLS } from "@constants";
import { HomePage } from "@pages/HomePage/HomePage";
import { NotFoundPage } from "@pages/NotFoundPage/NotFoundPage";
import { Layout as DefaultLayout } from "@common/Layouts";

const routes: { path: string; Component: { (): JSX.Element; Layout?: FC<{ children: ReactNode }> } }[] = [
    { path: URLS.DEFAULT_PAGE_URL, Component: HomePage },
    { path: "*", Component: NotFoundPage },
];

export const Routes = () => (
    <Switch>
        {routes.map(({ path, Component }) => {
            const { Layout = DefaultLayout } = Component;

            return (
                <Route
                    key={path}
                    path={path}
                    element={
                        <Layout>
                            <Component />
                        </Layout>
                    }
                />
            );
        })}
    </Switch>
);
