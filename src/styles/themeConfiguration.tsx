import { createTheme, responsiveFontSizes, ThemeOptions } from "@mui/material/styles";

export const themeOptions: ThemeOptions = {};

export const theme = responsiveFontSizes(createTheme(themeOptions));
