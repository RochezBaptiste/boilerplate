import { Routes } from "./Routes";
import myStyle from "./style";
import { Container, Box } from "@mui/material";
import { ErrorBoundary } from "@common/ErrorBoundary";
import { AppProviders } from "./AppProviders";

export const App = () => {
    const { container } = myStyle;

    return (
        <AppProviders>
            <Box sx={myStyle.root}>
                <Container sx={container}>
                    <ErrorBoundary>
                        <Routes />
                    </ErrorBoundary>
                </Container>
            </Box>
        </AppProviders>
    );
};
