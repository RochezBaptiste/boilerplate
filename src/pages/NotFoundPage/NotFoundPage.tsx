import { Typography } from "@mui/material";

export const NotFoundPage = () => {
    return <Typography variant="h3">There's nothing here !</Typography>;
};
