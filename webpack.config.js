/* eslint-disable */
const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const PATH_SOURCE = path.join(__dirname, "./src");
const PATH_DIST = path.join(__dirname, "./dist");

module.exports = (env, argv) => {
    const ENV_CONFIG = require(`./env_${argv.mode}.json`);
    Object.keys(ENV_CONFIG).forEach((key) => (ENV_CONFIG[key] = JSON.stringify(ENV_CONFIG[key])));

    return {
        context: PATH_SOURCE,
        entry: [path.join(PATH_SOURCE, "./index.tsx")],
        resolve: {
            extensions: [".tsx", ".ts", ".js"],
            alias: {
                "@common": path.join(PATH_SOURCE, "./common"),
                "@utils": path.join(PATH_SOURCE, "./utils"),
                "@features": path.join(PATH_SOURCE, "./features"),
                "@pages": path.join(PATH_SOURCE, "./pages"),
                "@static": path.join(PATH_SOURCE, "./static"),
                "@hooks": path.join(PATH_SOURCE, "./hooks"),
                "@constants": path.join(PATH_SOURCE, "./constants.ts"),
                "@styles": path.join(PATH_SOURCE, "./styles"),
                "@interfaces": path.join(PATH_SOURCE, "./interfaces"),
            },
        },
        output: {
            filename: "js/[name].[contenthash].js",
            publicPath: "/",
        },
        module: {
            rules: [
                {
                    test: /\.ts(x?)$/,
                    exclude: /node_modules\/.*/,
                    use: [
                        {
                            loader: "babel-loader",
                            options: {
                                configFile: path.resolve("babel.config.js"),
                            },
                        },
                    ],
                },
                {
                    test: /\.svg$/,
                    use: [
                        "@svgr/webpack",
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[ext]",
                                outputPath: "img",
                            },
                        },
                    ],
                },
                {
                    test: /\.(gif|png|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "img",
                    },
                },
                {
                    test: /\.(ttf|otf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "fonts",
                    },
                },
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                "process.env": {
                    APP_VERSION: JSON.stringify(require("./package.json").version),
                    ...ENV_CONFIG,
                },
            }),
            new HtmlWebpackPlugin({
                favicon: "../public/favicon.ico",
                template: path.join(PATH_SOURCE, "./index.html"),
                hash: true,
            }),
        ],
        devServer: {
            static: PATH_DIST,
            host: "localhost",
            port: 8080,
            historyApiFallback: true,
            open: true,
        },
    };
};
