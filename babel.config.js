/* eslint-disable */

const plugins = [
    [
        "babel-plugin-import",
        {
            libraryName: "@mui/material",
            libraryDirectory: "",
            camel2DashComponentName: false,
        },
        "core",
    ],
    [
        "babel-plugin-import",
        {
            libraryName: "@mui/icons-material",
            libraryDirectory: "",
            camel2DashComponentName: false,
        },
        "icons",
    ],
];

module.exports = function (api) {
    return {
        presets: [
            [
                "@babel/preset-env",
                {
                    corejs: 3,
                    useBuiltIns: "usage",
                },
            ],
            "@babel/preset-typescript",
            [
                "@babel/preset-react",
                {
                    runtime: "automatic",
                },
            ],
        ],
        plugins: ["@babel/plugin-proposal-class-properties"],
        ...(!api.env("test") && {
            plugins,
            ignore: ["**/*.test.ts", "**/*.test.tsx"],
        }),
    };
};
