/* eslint-disable */
module.exports = {
    rootDir: "./",
    roots: ["<rootDir>"],
    verbose: true,
    testURL: "http://localhost/",
    transform: {
        "^.+\\.(t|j)sx?$": "babel-jest",
    },
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    setupFilesAfterEnv: ["<rootDir>/testConfiguration/jest.setup.ts"],
    testPathIgnorePatterns: [
        "<rootDir>/dist/",
        "<rootDir>/node_modules/",
    ],
    moduleDirectories: [
        "node_modules",
        "testConfiguration"
    ],
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/testConfiguration/jestFileMock.ts",
    },
};
